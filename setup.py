from setuptools import setup, find_packages

setup(
    name='mydemo',
    version='0.1',
    packages=find_packages(exclude=['tests*']),
    license='MIT',
    description='An example python package',
    long_description=open('README.txt').read(),
    install_requires=['numpy'],
    url='https://gitlab.com/Oris/mydemo/-/edit/mastere',
    author='Oris'
)